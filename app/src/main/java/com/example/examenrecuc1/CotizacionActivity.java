package com.example.examenrecuc1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {

    TextView lblPagoMensualidad;
    TextView lblEnganche;


    EditText descripcion;
    EditText valorAutomovil;
    EditText porcentajePagoInicial;

    RadioGroup radioGroup;

    Button btnCalcular;
    Button btnLimpiar;
    Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);

        lblPagoMensualidad = findViewById(R.id.lblPagoMensualidad);
        lblEnganche = findViewById(R.id.lblEnganche);

        descripcion = findViewById(R.id.descripcion);
        valorAutomovil = findViewById(R.id.valorAutomovil);
        porcentajePagoInicial = findViewById(R.id.porcentajePagoInicial);

        radioGroup = findViewById(R.id.radioGroup);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        TextView textViewNombreCliente = findViewById(R.id.lblCliente);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            // Verificar si el Bundle contiene la clave "nombreUsuario"
            if (extras.containsKey("nombreCliente")) {
                // Obtener el valor de "nombreUsuario" del Bundle
                String nombreCliente = extras.getString("nombreCliente");

                textViewNombreCliente.setText("Cliente: " + nombreCliente);
            }
        }


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (validarInputs()){
                    double precioAutomovil = Double.parseDouble(valorAutomovil.getText().toString().trim());
                    double porcentaje = Double.parseDouble(porcentajePagoInicial.getText().toString().trim());
                    double porcentajeFormateado = porcentaje / 100;
                    int meses = 0;

                    int radioButtonId = radioGroup.getCheckedRadioButtonId();

                    if (radioButtonId == R.id.radioBtn1) {
                        // Opcion seleccionada
                        meses = 12;

                    } else if (radioButtonId == R.id.radioBtn2) {
                        // pcion seleccionada
                        meses = 18;

                    } else if (radioButtonId == R.id.radioBtn3) {
                        // opcion seleccionada
                        meses = 24;

                    } else if (radioButtonId == R.id.radioBtn4) {
                        // opcion seleccionada
                        meses = 36;
                    }

                    Cotizacion cotizacion = new Cotizacion();

                    double enganche = cotizacion.generarEnganche(porcentajeFormateado, precioAutomovil);
                    double mensualidades = cotizacion.generarMensualidad(meses);
                    //double totalPagar = subtotal - impuesto;

                    lblEnganche.setText("El Engance es de: $" + enganche);
                    lblPagoMensualidad.setText("El pago de la mensualidad es de: $" + mensualidades);
                }
            }
        });



        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descripcion.setText("");
                valorAutomovil.setText("");
                porcentajePagoInicial.setText("");
                radioGroup.clearCheck();

                lblPagoMensualidad.setText("El pago de la mensualidad es de: ");
                lblEnganche.setText("El Engance es de: ");
            }
        });


        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CotizacionActivity.this);
                builder.setTitle("Confirmación");
                builder.setMessage("¿Estás seguro de querer regresar?");
                builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Acciones a realizar si se selecciona "Sí"
                        Intent intent = new Intent(CotizacionActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish(); // Finaliza la actividad actual (CalculadorActivity)
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Acciones a realizar si se selecciona "No"
                        dialog.dismiss(); // Cierra el diálogo sin realizar ninguna acción adicional
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

    }


    public boolean validarInputs() {

        String descripciontxt = descripcion.getText().toString().trim();
        String valorAutomoviltxt = valorAutomovil.getText().toString().trim();
        String porcentajePagoInicialtxt = porcentajePagoInicial.getText().toString().trim();

        int radioButtonId = radioGroup.getCheckedRadioButtonId();


        if (!descripciontxt.isEmpty() && !valorAutomoviltxt.isEmpty() && !porcentajePagoInicialtxt.isEmpty() && radioButtonId != -1) {
            // Todos los campos están llenos y se ha seleccionado un RadioButton
            return true;
        } else {
            Toast.makeText(getApplicationContext(), "Todos los campos son requeridos", Toast.LENGTH_SHORT).show();
            return false;
        }


    }

}