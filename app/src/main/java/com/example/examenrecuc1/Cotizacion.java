package com.example.examenrecuc1;

public class Cotizacion {
    double enganche;
    double costoFinal;
    double pagoMensual;

    public Cotizacion(){
        this.enganche = 0.0;
        this.costoFinal = 0.0;
        this.pagoMensual = 0;
    }

    public double generarEnganche(double porcentajePagoInicial, double costoTotal){

        this.enganche = costoTotal * porcentajePagoInicial;

        this.costoFinal = costoTotal - this.enganche;

        return this.enganche;

    }

    public double generarMensualidad(int meses){
        this.pagoMensual = this.costoFinal / meses;

        return pagoMensual;
    }

}
